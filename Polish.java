/*
 * File: Polish.java
 *
 * Machine Problem 1
 *
 * Members: Rosheil Parel
 *          Grygjeanne Grace Icay
 *          Anfernee Sodusta
 */

import java.util.*;

class Polish {
    public static void main(String[] args) {
        String[]  caseNum = new String[10];
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter how many cases");

        int casee = scan.nextInt();

        for (int i = 0; i < casee; i++) {
            System.out.println("Please enter case #"+i+"");
            caseNum[i] = scan.nextLine();
        }
    }

}
