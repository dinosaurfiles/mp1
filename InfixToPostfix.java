import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Infix to Postfix using Linked Stack
 * @author Dorothy Anne G. Buenaventura
 * 
 */

public class InfixToPostfix {

	LinkedStack ls = new LinkedStack();	
	
	public InfixToPostfix(String string) {
		string = "( " + string + " )";
		StringTokenizer exp = new StringTokenizer(string);
		String postfix = "", perChar = "";
		int c = exp.countTokens();	
		for(int i =0; i < c; i++) {
			perChar = exp.nextToken();
			postfix += solveFix(perChar);
			}
		System.out.println("\nThe postfix value of " + 
			string.substring(1, string.length()-1) + " is:");
		System.out.println(postfix);
	}
	
	private String solveFix(String var){
		String val = "";
		if(isInt(var)){
			val += " " + var;
		}
		else if(var.equals("(")){
			ls.push(var);
		}
		else if(var.equals(")")){			
			while( ls.top().equals( "(" ) == false )
				val += " " + ls.pop();
			ls.pop();
		}
		else if(isOperator(var)){
			while( getICP(var) < getISP(ls.top()) )
				val += " " + ls.pop();	
			if(getICP(var) > getISP(ls.top()) )
				ls.push(var);
		}
		return val;
	}				
	
	private int getICP(Object x){	
		int icp;
		if(x.equals("+") || x.equals("-") )
			icp = 1;
		else if(x.equals("*")|| x.equals("/"))
			icp = 3;
		else if(x.equals("^"))
			icp = 6;
		else
			icp = 0;
		return icp;
	}
	
	private int getISP(Object x){
		int isp;
		if(x.equals("+") || x.equals("-") )
			isp = 2;
		else if(x.equals("*")|| x.equals("/"))
			isp = 4;
		else if(x.equals("^"))
			isp = 5;
		else
			isp = 0;
		return isp;
	}
	
	private boolean isOperator(String string){
		boolean exp = false;
		if(string.equals("+") || string.equals("-") || string.equals("*") 
				|| string.equals("/") || string.equals("^") )
					exp = true;
		return exp;
	}
	
	private boolean isInt(String string){
		boolean exp = true;
		try {
			Integer.parseInt(string);
		}
		catch(NumberFormatException exception){
			exp = false;
		}
		return exp;
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {			
		System.out.println("Input the infix expression " 
				+ "(operands, operators are separated by spaces):");
		Scanner input = new Scanner(System.in);
		String infixExpression = input.nextLine();
		InfixToPostfix calculator = new InfixToPostfix(infixExpression);
		//System.out.println("The equivalent postfix of the expression is " + 
			//	calculator.InfixToPostfix(infixExpression));
		input.close();
	}
		
}
