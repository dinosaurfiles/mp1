import java.util.*;
import java.io.*;

/**
 * File: Polish.java
 * Machine Problem # 1
 *
 * Members: Rosheil Parel
 *          Grygjeanne Grace Icay
 *          Anfernee Sodusta
 */

/**
 * Main class of Polish.java
 */
public class Polish {
    /**
     * Initialize Linked Stack
     */
    LinkedStack ls = new LinkedStack();

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter file name > ");
        String filename = scan.nextLine();
        Polish cases    = new Polish(filename);
    }

    /**
     * Main Polish method. Gets the filename and proceeds to the conversion of infix to postfix
     * @param The path or the filename of the txt
     */
    public Polish(String filename){
        try{
            File file = new File(filename);
            FileReader     fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String infixcase  = "";
            int    expcounter = Integer.parseInt(br.readLine()) + 1;
            int    counter    = 1;

            do {
                infixcase = br.readLine();

                infixTopostfix(sanitize(infixcase), infixcase, counter);

                counter++;
            } while (counter < expcounter);
        }catch (Exception e) {}
    }

    /**
     * Main method that converts infix to its postfix form
     * @param String[] exp. Array that contains seperated operators and operands of the expression
     * @param String infixcase. The original form of the expression
     * @param int counter. Counter for the expression
     */
    private void infixTopostfix(String[] exp, String infixcase, int counter){
        String[] operators = {"&", "|", "==", "!=", "<", "<=", ">", ">=", "+", "-", "–", "*", "/", "%", "^", "!"};
        ArrayList<String> postfixFinal = new ArrayList<String>();

        for (String c : exp) {
            if (Character.isLetter(c.charAt(0)) && Character.isLowerCase(c.charAt(0))) {
                postfixFinal.add(c);
            }else if (c.equals("(")) {
                ls.push(c);
            }else if (c.equals(")")) {
                while (ls.peek().equals("(") == false) {
                    postfixFinal.add(String.valueOf(ls.pop()));
                }

                ls.pop();
            }else if (!String.valueOf(operators).contains(c)) {

                if (ls.isEmpty()) {
                    ls.push(c);
                }else if (getISP(String.valueOf(ls.peek())) > getICP(c)) {
                    while (ls.isEmpty() != true && getISP(String.valueOf(ls.peek())) > getICP(c)) {
                        postfixFinal.add(String.valueOf(ls.pop()));
                    }

                    ls.push(c);
                }else{
                    ls.push(c);
                }
            }
        }

        //Pop all remaining operators
        while (ls.isEmpty() != true) {
            postfixFinal.add(String.valueOf(ls.pop()));
        }

        //Print out infix and postfix
        System.out.println("\nExpression " + counter + ":");
        System.out.println("Infix: " + infixcase);
        System.out.println("Postfix: " + String.join(" ", postfixFinal) + "\n");

    }

    /**
     * Method that seperates all operators and operands. Also removes whitespaces so that operands
     *  and operators should remain
     * @param String xp. The original expression
     * @return returns the sanitized expression into String Array
     */
    private String[] sanitize(String xp){
        String[] result = xp.split("(?<=[() ])|(?=[() ])");
        ArrayList<String> strManipulate = new ArrayList<String>();

        for (String s : result) {
            if (s.equals(" ") != true) {
                strManipulate.add(s);
            }
        }

        return (strManipulate.toArray(new String[strManipulate.size()]));

    }

    /**
     * Method that gets the incoming priority number of the operators
     * @param String x. The operator
     * @return returns the incoming priority number
     */
    private int getICP(String x){
        int icp;

        if (x.equals("!")) {
            icp = 15;
        } else if (x.equals("^")) {
            icp = 13;
        } else if (x.equals("*") || x.equals("/") || x.equals("%")) {
            icp = 10;
        } else if (x.equals("+") || x.equals("-") || x.equals("–")) {
            icp = 8;
        } else if (x.equals("<") || x.equals(">") || x.equals("<=") || x.equals(">=")) {
            icp = 6;
        } else if (x.equals("==") || x.equals("!=")) {
            icp = 3;
        } else if (x.equals("&") || x.equals("|")) {
            icp = 1;
        } else{
            icp = 0;
        }

        return icp;
    }

    /**
     * Method that gets the in stack priority number of the operators
     * @param String x. The operator
     * @return returns the in stack priority number
     */
    private int getISP(String x){
        int isp;

        if (x.equals("!")) {
            isp = 14;
        } else if (x.equals("^")) {
            isp = 12;
        } else if (x.equals("*") || x.equals("/") || x.equals("%")) {
            isp = 11;
        } else if (x.equals("+") || x.equals("-") || x.equals("–")) {
            isp = 9;
        } else if (x.equals("<") || x.equals(">") || x.equals("<=") || x.equals(">=")) {
            isp = 7;
        } else if (x.equals("==") || x.equals("!=")) {
            isp = 4;
        } else if (x.equals("&") || x.equals("|")) {
            isp = 2;
        } else{
            isp = 0;
        }

        return isp;
    }

}
