/**
 * Implementation of the LinkedStack
 * Main class of the LinkedStack
 */
public class LinkedStack {
    private Node top;
    private int  size;

    /**
     * Initialize the Linked Stack as Empty and also the size
     */
    public LinkedStack(){
        top  = null;
        size = 0;
    }

    /**
     * Checks if stack is Empty; returns true if empty
     * @return. true or false
     */
    public boolean isEmpty(){
        return (top == null);
    }

    /**
     * Returns the size of the stack
     * @return. size of stack
     */
    public int getSize(){
        return size;
    }

    /**
     * Method to push an element to the stack and increment its size
     */
    public void push(Object ob){
        top = new Node(ob, top);
        size++;
    }

    /**
     * Method to pop the topmost element of the stack and decrement the size
     * @return. Returns the topmost Object/Element
     */
    public Object pop(){
        if (isEmpty()) {
            throw new RuntimeException("Stack error: stack empty");
        }

        Object currTop = top.getElement();
        top = top.getNext();
        size--;
        return currTop;
    }

    /**
     * Method to view the top element of the stack
     * @return. Returns the topmost Element/Object
     */
    public Object peek(){
        if (isEmpty()) {
            throw new RuntimeException("Stack error: stack empty");
        }

        return top.getElement();
    }

    /**
     * Method to display the all the elements inside the stack
     */
    public void display(){
        System.out.print("\nStack = ");

        if (size == 0) {
            System.out.print("Empty\n");
            return;
        }

        Node pointer = top;

        while (pointer != null) {
            System.out.print(pointer.getElement() + " ");
            pointer = pointer.getNext();
        }

        System.out.println();
    }

}
