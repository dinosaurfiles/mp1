/**
 * Implementation of the LinkedStack. Where the storage and linking is happening
 * Main class of the where the linking of the class happens
 */
public class Node {
    private Object element;
    private Node   next;

    /**
     * Constructor  for the node Class
     */
    public Node(Object e, Node n){
        element = e;
        next    = n;
    }

    /**
     * Points to the next node/element of element(e.g. newElement.next() )
     */
    public void nextNode(Node n){
        next = n;
    }

    /**
     * Set the current node
     */
    public void currNode(int e){
        element = e;
    }

    /**
     * Return the next Element of the current Node/Element
     */
    public Node getNext(){
        return next;
    }

    /**
     * Return the current Element of the Node
     */
    public Object getElement(){
        return element;
    }

}
