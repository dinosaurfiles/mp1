import java.util.*;
import java.io.*;

public class Demo {
    LinkedStack ls = new LinkedStack();
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter file name > ");
        String filename = "input.txt";
        Demo   cases    = new Demo(filename);
    }

    public Demo(String filename){
        try{
            File file = new File(filename);
            FileReader     fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String infix = "";
            int    noOfExpressions = Integer.parseInt(br.readLine()) + 1;
            int    counter = 1;

            do {
                infix = br.readLine();

                infixTopostfix(sanitize(infix));

                counter++;
            } while (counter < noOfExpressions);
        }catch (Exception e) {}
    }

    private void infixTopostfix(String[] exp){
        String[] operators = {"&", "|", "==", "!=", "<", "<=", ">", ">=", "+", "-", "–", "*", "/", "%", "^", "!"};
        ArrayList<String> postfixFinal = new ArrayList<String>();

        for (String c : exp) {
            if (Character.isLetter(c.charAt(0))) {
                postfixFinal.add(c);
            }else if (c.equals("(")) {
                ls.push(c);
            }else if (c.equals(")")) {
                while (ls.peek().equals("(") == false) {
                    postfixFinal.add(String.valueOf(ls.pop()));
                }

                ls.pop();
            }else if (!String.valueOf(operators).contains(c)) {

                if (ls.isEmpty()) {
                    ls.push(c);
                }else if (getISP(String.valueOf(ls.peek())) > getICP(c)) {
                    while (ls.isEmpty() != true && getISP(String.valueOf(ls.peek())) > getICP(c)) {
                        postfixFinal.add(String.valueOf(ls.pop()));
                    }

                    ls.push(c);
                }else{
                    ls.push(c);
                }
            }
        }

        while (ls.isEmpty() != true) {
            postfixFinal.add(String.valueOf(ls.pop()));
        }

        System.out.println(postfixFinal);

    }

    private String[] sanitize(String xp){
        String[] result = xp.split("(?<=[() ])|(?=[() ])");
        ArrayList<String> strManipulate = new ArrayList<String>();

        for (String s : result) {
            if (s.equals(" ") != true) {
                strManipulate.add(s);
            }
        }

        return (strManipulate.toArray(new String[strManipulate.size()]));

    }

    private int getICP(String x){
        int icp;

        if (x.equals("!")) {
            icp = 15;
        } else if (x.equals("^")) {
            icp = 13;
        } else if (x.equals("*") || x.equals("/") || x.equals("%")) {
            icp = 10;
        } else if (x.equals("+") || x.equals("-") || x.equals("–")) {
            icp = 8;
        } else if (x.equals("<") || x.equals(">") || x.equals("<=") || x.equals(">=")) {
            icp = 6;
        } else if (x.equals("==") || x.equals("!=")) {
            icp = 3;
        } else if (x.equals("&") || x.equals("|")) {
            icp = 1;
        } else{
            icp = 0;
        }

        return icp;
    }

    private int getISP(String x){
        int isp;

        if (x.equals("!")) {
            isp = 14;
        } else if (x.equals("^")) {
            isp = 12;
        } else if (x.equals("*") || x.equals("/") || x.equals("%")) {
            isp = 11;
        } else if (x.equals("+") || x.equals("-") || x.equals("–")) {
            isp = 9;
        } else if (x.equals("<") || x.equals(">") || x.equals("<=") || x.equals(">=")) {
            isp = 7;
        } else if (x.equals("==") || x.equals("!=")) {
            isp = 4;
        } else if (x.equals("&") || x.equals("|")) {
            isp = 2;
        } else{
            isp = 0;
        }

        return isp;
    }

}
