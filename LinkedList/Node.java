public class Node {
    private Object element;  //Acts as a storage for all the Elements in the stack
    private Node   next;  //stores here the next Element

    /*  Constructor  for the node Class*/
    public Node(Object e, Node n){
        element = e;
        next = n;
    }

    /*  points to the next node ( newElement.next )  */
    public void nextNode(Node n){
        next = n;
    }

    /*  set to the current node */
    public void currNode(int e){
        element = e;
    }

    /*  return the next Element of the current Node  */
    public Node getNext(){
        return next;
    }

    /*  return the current Element of the Node */
    public Object getElement(){
        return element;
    }

}
