import java.util.*;

public class LinkedStackImplement {

    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);

        /* Creating object of class LinkedStack */

        LinkedStack ls = new LinkedStack();

        ls.push('a');
        ls.push('+');
        ls.push('b');
        ls.push('-');
        ls.push('c');
        ls.push('/');
        ls.push('d');
        ls.push('+');
        ls.push('e');
        ls.push('^');
        ls.push('f');
        ls.push('^');
        ls.push('g');
        System.out.println(ls.getSize());
        System.out.println(ls.peek());
        System.out.println(ls.isEmpty());
        ls.display();
        /*
           Stack Operations:
           ls.push([])
           ls.pop()
           ls.peek()
           ls.isEmpty()
           ls.getSize()
           ls.display()
         */
    }

}
