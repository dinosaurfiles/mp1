public class LinkedStack {
    private Node top;
    private int  size;

    /*  Initialize Linked Stack as Empty and also the size */
    public LinkedStack(){
        top  = null;
        size = 0;
    }

    /*  Checks if stack is Empty; returns true if empty, false if not */
    public boolean isEmpty(){
        return (top == null);
    }

    /*  Returns the size of the stack */
    public int getSize(){
        return size;
    }

    /*  Function to push an element to the stack and incrementing the size */
    public void push(Object ob){
        top = new Node(ob, top);
        size++;
    }

    /*  Function to pop an element from the stack. returns the previous top and decrementing the size*/
    public Object pop(){
        if (isEmpty()) {
            throw new RuntimeException("Stack error: stack empty");
        }

        Object currTop = top.getElement();
        top = top.getNext();
        size--;
        return currTop;
    }

    /*  Function to view the top element of the stack and returns it*/
    public Object peek(){
        if (isEmpty()) {
            throw new RuntimeException("Stack error: stack empty");
        }

        return top.getElement();
    }

    /*  display the all the elements inside the stack*/
    public void display(){
        System.out.print("\nStack = ");

        if (size == 0) {
            System.out.print("Empty\n");
            return;
        }

        Node pointer = top;

        while (pointer != null) {
            System.out.print(pointer.getElement() + " ");
            pointer = pointer.getNext();
        }

        System.out.println();
    }

}
