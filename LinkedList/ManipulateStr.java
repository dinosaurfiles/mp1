import java.util.*;
import java.io.*;
public class ManipulateStr {
    public static void main(String[] args) {
        String case1 = "a + b - c / d + e ^ f ^ g";
        String case2 = "(a - (b ^ (c % d) | e))";
        String case3 = "(a >= b) < c != ! b";

        //ArrayList<String> postfixFinal = new ArrayList<String>();
        //String[] result1 = case1.split("[^\s()*/%+-]+");
        //System.out.println(Arrays.toString(result1));
        //String[] result1  = case3.split("(?<=(<=|>=|!=))|(?=(<=|>=|!=))|(?<=[() ])|(?=[() ])");
        String[] result1 = case1.split("(?<=[() ])|(?=[() ])");
        //System.out.println(Arrays.toString(result1));
        ArrayList<String> posst = new ArrayList<String>();

        /*
        for (String s : result1) {
            if (s.equals(" ") != true) {
                System.out.println("[" + s + "]");
                posst.add(s);
            }
        }

        System.out.println(posst);
        */

        String filename = ("input.txt");
        try{
            File file = new File(filename);
            FileReader     fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String infix = "";
            int    noOfExpressions = Integer.parseInt(br.readLine()) + 1;
            int    counter = 1;

            do {
                infix = br.readLine();
                System.out.println(infix);
                counter++;
            } while (counter < noOfExpressions);
        }
        catch (Exception e) {}


    }

}
